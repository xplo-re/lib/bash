#!/bin/bash

declare -i tests_completed=0
declare -i tests_failed=0

is_equal_to()
{
	local stdin
	local expected="$1"
	
	IFS= read -r stdin
	
	test "$stdin" = "$expected"
}

test_init()
{
	tests_completed=0
	tests_failed=0
    
    echo $(colorize "[$1]" yellow bold)
}

test_summary()
{
    if test $tests_failed -gt 0;
    then
        if test $tests_failed = $tests_completed;
        then
            color=red
        else
            color=yellow
        fi
    else
        color=green
    fi
    
	echo $(colorize "$tests_failed of $tests_completed failed" $color bold)
	
	test $tests_failed = 0
}

test_expect_success()
{
	local retval=$?
	local result
	local color
	
	tests_completed=$(( tests_completed + 1 ))
	
	if test $retval = 0;
	then
		color=green
		result="OK"
	else
		color=red
		tests_failed=$(( tests_failed + 1 ))
		result="FAIL"
	fi

	echo $(colorize "$1: $result" $color)
}

test_expect_failure()
{
	local retval=$?
	
	test $retval != 0
	test_expect_success "$1"
}

test_expect_retval()
{
	local retval=$?
	
	test $retval = "$2"
	test_expect_success "$1"
}

colorize()
{
	local color
    local text="$1"
	shift

	if test "$TERM";
	then
		# Output is a terminal, check for number of colours supported.
		local colors=$(tput colors)

	    if test -n "$colors" && test $colors -ge 8;
		then
            local sequence;
            
            while test $# -gt 0;
            do
                case "$1" in
	                0|black)        sequence="$sequence$(tput setaf 0)";;
                    1|red)          sequence="$sequence$(tput setaf 1)";;
                    2|green)        sequence="$sequence$(tput setaf 2)";;
                    3|yellow)       sequence="$sequence$(tput setaf 3)";;
                    4|blue)         sequence="$sequence$(tput setaf 4)";;
                    5|magenta)      sequence="$sequence$(tput setaf 5)";;
                    6|cyan)         sequence="$sequence$(tput setaf 6)";;
                    7|white)        sequence="$sequence$(tput setaf 7)";;
                    b|bold)         sequence="$sequence$(tput bold)";;
                    u|underline)    sequence="$sequence$(tput smul)";;
                    h|highlight)    sequence="$sequence$(tput smso)";;
                esac
                
                shift
            done
            
            echo -n "$sequence$text$(tput sgr0)"
            return 0
	    fi
	fi
    
	echo -n "$text"
}

test_run()
{
    test_init "test framework"
    test_expect_success "is_equal_to simple 1" `echo -n "ABC" | is_equal_to "ABC"`
    test_expect_success "is_equal_to simple 2" `echo -n "AbC" | is_equal_to "AbC"`
    test_expect_failure "is_equal_to simple 3" `echo -n "AbC" | is_equal_to "abc"`
    test_expect_failure "is_equal_to multiline 1" `echo -n "123\n\r4" | is_equal_to "123"`
    test_expect_success "is_equal_to multiline 2" `echo -n "123\n\r4" | is_equal_to "123\n\r4"`
    test_expect_success "is_equal_to binary 1" `echo -n "123\r\n4" | is_equal_to "123\r\n4"`
    test_expect_success "is_equal_to binary 2" `echo -n "123\r\n4\t5" | is_equal_to "123\r\n4\t5"`
    test_expect_success "is_equal_to binary 3" `echo -n "123\r\n4\t5 " | is_equal_to "123\r\n4\t5 "`
    test_expect_failure "is_equal_to binary 4" `echo -n "123\r\n4\t5 " | is_equal_to "123\r\n4\t5"`
    test_expect_success "is_equal_to binary 5" `echo -n " 123\r\n4\t5" | is_equal_to " 123\r\n4\t5"`
    test_expect_failure "is_equal_to binary 6" `echo -n " 123\r\n4\t5" | is_equal_to "123\r\n4\t5"`
    test_summary
}