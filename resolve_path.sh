#!/bin/bash
# -*- Mode: Shell Script; tab-width: 4; indent-tabs-mode: on; coding: utf-8 -*-

# Canonicalises path by resolving symbolic links.
resolve_path()
{
	local resolved="$1"
	local target

	while test -L "$resolved";
	do
		target=$(expr "`ls -ld "$resolved"`" : '.*-> \(.*\)$')

		if expr "$target" : '/.*' >/dev/null;
		then
			resolved="$target"
		else
			# Prepend path information for link within same directory.
			resolved="$(dirname "$resolved")/$target"
		fi
	done

	echo "$resolved"
}